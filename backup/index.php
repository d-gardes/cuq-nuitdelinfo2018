<!DOCTYPE html>
<html>
<head>
	<?php include "assets/php/meta.php";?>
</head>
<body>

	<div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				<div class="left-menu text-center">
					<ul>
						<li onclick="switchPage()"><i class="fas fa-sync-alt"></i></li>
					</ul>
					<ul class="page1-left">
						<li onclick="toggle('food-menu')"><i class="fas fa-utensils"></i></li>
						<li onclick="toggle('stock-menu')"><i class="fas fa-archive"></i></li>
					</ul>
					<ul class="page2-left">
						<li onclick="toggle('camera-menu')"><i class="fas fa-camera"></i></i></li>
						<li onclick="toggle('weather-menu')"><i class="fas fa-sun"></i></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="center text-center">
					<div id="food-menu" class="food-div text-center">
						<h1>Food</h1>
						<ul>
							<li>Steak : 1KG</li>
							<li>Frites : 250KG</li>
							<li>Sauche Blanche : 800L</li>
						</ul>
					</div>
					<div id="stock-menu" class="stock-div text-center">
						<h1>Stock</h1>
						<ul>
							<li>Batterie : 20</li>
							<li>Pneus : 2</li>
							<li></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="right-info text-center">
					<ul class="page1-right">
						<li>
							<div class="geopos">
								<p>-21°03'22"</p>
								<p>17°32'24"</p>
							</div>
						</li>
						<li><i class="fas fa-tint"></i></li>
						<li><i class="fas fa-thermometer-three-quarters"></i></li>
						<li><i class="fas fa-wind"></i></li>
					</ul>
					<ul class="page2-right">
						<li><i class="far fa-calendar-check"></i></li>
						<li><i class="fas fa-thermometer-three-quarters"></i></li>
						<li><i class="fas fa-wind"></i></li>
					</ul>
				</div>
			</div>
		</div>
		
	</div>

</body>
	
	<script>
		
		initFirst();

	</script>

</html>