<style>
	
	


	ul{
		list-style-type: none;
	}

	body{
		background-image: url('assets/img/background.jpg.png');
		background-position: center;
		background-repeat: no-repeat;
	}

	*{
		font-family: 'Montserrat', sans-serif;
	}

	/*
	 *
	 *   Hide stuff and others
	 *
	 */

	.hide, .hide > *{
		width: 0% !important;
		visibility: hidden;
		font-size: 0px;
		position: absolute;
	}

	.hide-page-left{
		transform: translateX(-50vh);
		color: rgba(255, 255, 255, 0);
		width: 0%;
		visibility: hidden;
		position: absolute;
	}
	.hide-page-right{
		transform: translateX(50vh);
		color: rgba(255, 255, 255, 0);
		width: 0%;
		visibility: hidden;
		position: absolute;
	}

	.page1-left{
		-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
	}.page2-right{
-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
		}.page2-left{
-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
			}.page1-right{
-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
			}

	.stock-div{
		-webkit-clip-path: polygon(19% 0, 64% 0, 94% 0, 100% 86%, 86% 100%, 8% 100%, 0 17%);
		clip-path: polygon(19% 0, 64% 0, 94% 0, 100% 86%, 86% 100%, 8% 100%, 0 17%);
		-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
		color: #FFF;
		font-weight: 500;
		background-image: linear-gradient(90deg, rgba(0, 255, 102, 0.45), rgba(0, 255, 0, 0.21), rgba(255, 0, 0, 0.34));
		background-color: rgba(0, 255, 0, 0.28);
		margin: auto;
		width: 60%;
		margin-top: 40vh;
		}.stock-div ul{
			padding: 0;
		}.stock-div ul li{
			font-size: 36px;
		}.stock-div h1{
			font-size: 48px;
		}

	.food-div{
		-webkit-clip-path: polygon(19% 0, 64% 0, 94% 0, 100% 86%, 86% 100%, 8% 100%, 0 17%);
		clip-path: polygon(19% 0, 64% 0, 94% 0, 100% 86%, 86% 100%, 8% 100%, 0 17%);
		-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
		color: #FFF;
		font-weight: 500;
		background-image: linear-gradient(90deg, rgba(0, 255, 102, 0.45), rgba(0, 255, 0, 0.21), rgba(255, 0, 0, 0.34));
		background-color: rgba(0, 255, 0, 0.28);
		margin: auto;
		width: 60%;
		margin-top: 40vh;
		}.food-div ul{
			padding: 0;
		}.food-div ul li{
			font-size: 36px;
		}.food-div h1{
			font-size: 48px;
		}

	.geopos{
		color: #FFF;
		font-size: 48px;
	}

	.left-menu{
		height: 100vh;
		background-color: rgba(0, 255, 0, 0.28);
		margin-left: -15px;
	}

	.right-info{
		height: 100vh;
		background-color: rgba(0, 255, 0, 0.28);
		margin-right: -15px;
	}

	.left-menu li{
		color: #FFF;
		font-size: 80px;
		padding-top: 12vh;
		padding-bottom: 12vh;
		-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
	}

	.right-info li{
		-webkit-transition: all .2s ease-in-out;
		-moz-transition: all .2s ease-in-out;
		-o-transition: all .2s ease-in-out;
		transition: all .2s ease-in-out;
	}

	.right-info i{
		color: #FFF;
		font-size: 80px;
		padding-top: 10vh;
		padding-bottom: 10vh;
	}

	.left-menu li:hover{
		transform: translateX(50px);
	}

	.right-info li:hover{
		transform: translateX(-50px);
	}


</style>