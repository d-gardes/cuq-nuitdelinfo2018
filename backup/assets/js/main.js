
var foodMenuEnabled = false;
var stockMenuEnabled = false;
var cameraMenuEnabled = false;
var weatherMenuEnabled = false;

var shown = 0;
var currentPage = 1;
var foodMenuElement;
var stockMenuElement;
var cameraMenuElement;
var weatherMenuElement;

function init(){
	foodMenuElement = document.getElementById("food-menu");
	foodMenuElement.classList.add("hide");
	foodMenuEnabled = false;

	stockMenuElement = document.getElementById("stock-menu");
	stockMenuElement.classList.add("hide");
	stockMenuEnabled = false;
}

function initFirst(){

	foodMenuElement = document.getElementById("food-menu");
	foodMenuElement.classList.add("hide");
	foodMenuEnabled = false;

	stockMenuElement = document.getElementById("stock-menu");
	stockMenuElement.classList.add("hide");
	stockMenuEnabled = false;

	removeClass("page1-left", "hide-page-left");
	removeClass("page1-right", "hide-page-right");
	addClass("page2-left", "hide-page-left");
	addClass("page2-right", "hide-page-right");
	currentPage = 1;
}

function toggle(id){
	if(id == "food-menu"){
		toggleFoodMenu();
		return;
	}
	if(id == "stock-menu"){
		toggleStockMenu();
		return;
	}
	if(id == "camera-menu"){
		toggleCameraMenu();
		return;
	}
}

function isSomethingOn(){
	return foodMenuEnabled  | stockMenuEnabled | cameraMenuEnabled | weatherMenuEnabled;
}

function toggleCameraMenu(){
	var cameraMenuElement = document.getElementById("stock-menu");
	if(cameraMenuEnabled){
		cameraMenuElement.classList.add("hide");
		cameraMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		cameraMenuElement.classList.remove("hide");
		cameraMenuEnabled = true;
	}
}

function toggleStockMenu(){
	var stockMenuElement = document.getElementById("stock-menu");
	if(stockMenuEnabled){
		stockMenuElement.classList.add("hide");
		stockMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		stockMenuElement.classList.remove("hide");
		stockMenuEnabled = true;
	}
}

function toggleFoodMenu(){
	var foodMenuElement = document.getElementById("food-menu");
	if(foodMenuEnabled){
		foodMenuElement.classList.add("hide");
		foodMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		foodMenuElement.classList.remove("hide");
		foodMenuEnabled = true;
	}
}


function switchPage(){
	if(currentPage == 1){
		addClass("page1-left", "hide-page-left");
		addClass("page1-right", "hide-page-right");
		removeClass("page2-left", "hide-page-left");
		removeClass("page2-right", "hide-page-right");

		currentPage = 2;
	}else{
		removeClass("page1-left", "hide-page-left");
		removeClass("page1-right", "hide-page-right");
		addClass("page2-left", "hide-page-left");
		addClass("page2-right", "hide-page-right");
		currentPage = 1;
	}
}

function addClass(paramClassPrim, paramClass){
	var classArray = document.getElementsByClassName(paramClassPrim);
	var i;
	for (i = 0; i < classArray.length; i++) {
		classArray[i].classList.add(paramClass);
	}
}

function removeClass(paramClassPrim, paramClass){
	var classArray = document.getElementsByClassName(paramClassPrim);
	var i;
	for (i = 0; i < classArray.length; i++) {
		classArray[i].classList.remove(paramClass);
	}
}
