<?php
class VoiceRSS
{
	public function speech($settings) {
	    $this->_validate($settings);
	    return $this->_request($settings);
	}

	private function _validate($settings) {
	    if (!isset($settings) || count($settings) == 0) throw new Exception('The settings are undefined');
        if (!isset($settings['key']) || empty($settings['key'])) throw new Exception('The API key is undefined');
        if (!isset($settings['src']) || empty($settings['src'])) throw new Exception('The text is undefined');
        if (!isset($settings['hl']) || empty($settings['hl'])) throw new Exception('The language is undefined');
	}

	private function _request($settings) {
    	$url = ((isset($settings['ssl']) && $settings['ssl']) ? 'https' : 'http') . '://api.voicerss.org/';
    	$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, (isset($settings['b64']) && $settings['b64']) ? 0 : 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_buildRequest($settings));

		$resp = curl_exec($ch);

		curl_close($ch);
		
		$is_error = strpos($resp, 'ERROR') === 0;
	    
    	return array(
    		'error' => ($is_error) ? $resp : null,
    		'response' => (!$is_error) ? $resp: null);
	}

	private function _buildRequest($settings) {
	    return http_build_query(array(
	        'key' => isset($settings['key']) ? $settings['key'] : '',
	        'src' => isset($settings['src']) ? $settings['src'] : '',
	        'hl' => isset($settings['hl']) ? $settings['hl'] : '',
	        'r' => isset($settings['r']) ? $settings['r'] : '',
	        'c' => isset($settings['c']) ? $settings['c'] : '',
	        'f' => isset($settings['f']) ? $settings['f'] : '',
	        'ssml' => isset($settings['ssml']) ? $settings['ssml'] : '',
	        'b64' => isset($settings['b64']) ? $settings['b64'] : ''
	    ));
	}
}
?>

    
<html xmlns="http://www.w3.org/1999/xhtml" id="generic">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FATAL 401 ERROR</title>
<link rel="stylesheet" type="text/css" media="all" href="/style.css"></style>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"> 
<script type="text/javascript">
var sc_project=8426490; 
var sc_invisible=1; 
var sc_security="e6fcfaae"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost +
"statcounter.com/counter/counter.js'></"+"script>");</script>
<noscript><div class="statcounter"><a title="web stats"
href="http://statcounter.com/free-web-stats/"
target="_blank" rel="external nofollow"><img class="statcounter"
src="https://c.statcounter.com/8426490/0/e6fcfaae/1/"
alt="web stats"></a></div></noscript>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36039573-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
function blinkFont()
{
  document.getElementById("blink").style.color="#00A"
  setTimeout("setblinkFont()",800)
}
function setblinkFont()
{
  document.getElementById("blink").style.color=""
  setTimeout("blinkFont()",800)
}
</script>
</head>
<body onload="blinkFont()">
<b>
<span class="neg">401 ERROR</span>
<p>
A problem has been detected and windows has been shut down to prevent damage to your computer.
</p>
<p>
*** STOP: 0xFFFFFFFF (0xFFFFFFFF, 0x00DF11DS, 0x01011011, 0x01110110).<br /><br />
*** ERROR CODE: 401 UNAUTHORIZED. The request was valid, but the server is refusing action. The authentication is required and has failed or has not yet been provided.<br /><br />

* Press any key to terminate the current application.<br />
* Press CTRL+ALT+DELETE again to restart your computer. You will lose any unsaved information in all applications.<br />

</p>
<center>Press any key to continue <span id="blink">_</span></center>
</b>
<style>
@charset "utf-8";
/* CSS Document */

/****** Generic *******/
html#home body, html#error404 body{
    background:#0000aa;
    color:#ffffff;
    font-family:courier;
    font-size:12pt;
    text-align:center;
    margin:100px;
}

html#generic {
	-webkit-user-select: none; /* Safari 3.1+ */
	-moz-user-select: none; /* Firefox 2+ */
	-ms-user-select: none; /* IE 10+ */
	user-select: none; /* Standard syntax */
	cursor: none; /* invisible cursor */
}

html#generic body {
    background:#0000aa;
    color:#ffffff;
    font-family:courier;
    font-size:12pt;
    text-align:center;
    margin:100px;
}

blink {
    color:yellow;
}

.neg {
    background:#fff;
    color:#0000aa;
    font-weight:bold;
}

span.neg{
    padding:2px 8px;
}

p {
    margin:30px 100px;
    text-align:left;
}

p.center {
    text-align:center;
}

a,a:hover {
    color:inherit;
    font:inherit;
	text-decoration: none;
}
</style>
</body>
</html>
<?php
$tts = new VoiceRSS;
$voice = $tts->speech([
    'key' => '7c578f5118ca44158c2136ffab51cc94',
    'hl' => 'en-us',
    'src' => '401 UNAUTHORIZED. The request was valid, but the server is refusing action. The authentication is required and has failed or has not yet been provided.',
    'r' => '0',
    'c' => 'mp3',
    'f' => '44khz_16bit_stereo',
    'ssml' => 'false',
    'b64' => 'true'
]);

echo '<audio src="' . $voice['response'] . '" autoplay="autoplay"></audio>';
?>