<!DOCTYPE html>
<html>
<head>
	<?php include "assets/php/meta.php";?>
</head>
<body>

	<div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2">
				<div class="left-menu text-center">
					<ul>
						<li onclick="switchPage()"><i class="fas fa-sync-alt"></i></li>
					</ul>
					<ul class="page1-left">
						<li onclick="toggle('food-menu')"><i class="fas fa-utensils"></i></li>
						<li onclick="toggle('stock-menu')"><i class="fas fa-archive"></i></li>
					</ul>
					<ul class="page2-left">
						<li onclick="toggle('camera-menu')"><i class="fas fa-camera"></i></i></li>
						<li onclick="toggle('weather-menu')"><i class="fas fa-sun"></i></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="center text-center">
					<div id="food-menu" class="food-div text-center">
						<h1>Food</h1>
						<ul>
							<li>Steak : 1KG</li>
							<li>Frites : 250KG</li>
							<li>Sauche Blanche : 800L</li>
						</ul>
					</div>
					<div id="stock-menu" class="stock-div text-center">
						<h1>Stock</h1>
						<ul>
							<li>Batterie : 20</li>
							<li>Pneus : 2</li>
							<li></li>
						</ul>
					</div>
					<div id="camera-menu" class="stock-div text-center">
						<h1>Camera</h1>
						<ul>
							<li>Batterie : 20</li>
							<li>Pneus : 2</li>
							<li></li>
						</ul>
					</div>
					
					<div id="checklist-menu" class="checklist-div text-center">
						<h1>Checklist</h1>
						<?php

							$bdd = new PDO('mysql:host=localhost;dbname=cuq-nuitinfo', 'root', 'nuitdelinfo');
							ini_set('display_errors',1);
							$list='';
							$list.='
							<h2>Vérification des besoins vitaux</h2>
							<form action=""> 
							  <select name="customers" onchange="showCustomer(this.value)">
							  	<option value=""> ... Veuillez sélectionner une Check-list ... </option>
							';
							  	foreach ($bdd->query('SELECT `Nom_liste` FROM `check_lists` GROUP BY `Nom_liste`') as $table){
									$list.='    <option value="'.$table['Nom_liste'].'" ';
									$list.='>'.$table['Nom_liste'].'</option>

								';
								}
							    $list.='</select>
							</form>
							<br>
							<div id="list"></div>
							<script>
							function showCustomer(str) {
							  var xhttp;  
							  if (str == "") {
							    document.getElementById("list").innerHTML = "";
							    return;
							  }
							  xhttp = new XMLHttpRequest();
							  xhttp.onreadystatechange = function() {
							    if (this.readyState == 4 && this.status == 200) {
							      document.getElementById("list").innerHTML = this.responseText;
							    }
							  };
							  xhttp.open("GET", "ajax_check_list.php?q="+str, true);
							  xhttp.send();
							}
							</script>';
							echo $list;
							?>
					</div>
					<div id="addlist-menu" class="stock-div text-center">
						<h1>Ajouter Check-list</h1>
						<form method=POST action=add_check_list.php >
        					<input type="text" name="Nom_liste" value="" placeholder="Nom de votre check-list" required />
        					<div class="tache">
        					    <input class="nb" type="text" name="nb1" value="1" readonly/>
        					    <input type="text" name="nom1" value="" placeholder="Nom de votre 1ère tache" required />
        					</div>
        					<div class="">
        					    <button class="btn btn-default"  type="submit">Valider</button>
        					</div>
							</form>
						<button onclick="addLine()">Add Line</button>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="right-info text-center">
					<ul class="page1-right">
						<li>
							<div class="geopos">
								<p>-21°03'22"</p>
								<p>17°32'24"</p>
							</div>
						</li>
						<li><i class="fas fa-tint"></i></li>
						<li><i class="fas fa-thermometer-three-quarters"></i></li>
						<li><i class="fas fa-wind"></i></li>
					</ul>
					<ul class="page2-right">
						<li onclick="toggle('checklist-menu')"><i class="far fa-calendar-check"></i></li>
						<li onclick="toggle('addlist-menu')"><i class="fas fa-plus-square"></i></li>
						<li><i class="fas fa-wind"></i></li>
					</ul>
				</div>
			</div>
		</div>
		
	</div>

</body>
	
	<script>
		
		initFirst();

	</script>

</html>