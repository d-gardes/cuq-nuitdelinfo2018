
var foodMenuEnabled = false;
var stockMenuEnabled = false;
var cameraMenuEnabled = false;
var weatherMenuEnabled = false;
var checklistMenuEnabled = false;
var addlistMenuEnabled = false;

var shown = 0;
var currentPage = 1;
var foodMenuElement;
var stockMenuElement;
var cameraMenuElement;
var weatherMenuElement;
var checkListMenuElement;
var addListMenuElement;

    var idEnCours;
    function addLine(){
        idEnCours ++;
        var div ="<div class=\"tache\">\
            <input type=\"text\" name=\"nb"+idEnCours+"\" value=\""+idEnCours+"\" readonly/>\
            <input class=\"nb\" type=\"text\" name=\"nom"+idEnCours+"\" value=\"\" placeholder=\"Nom de votre "+idEnCours+"nde tache\" required />\
        </div>";
        var tab_Taches = document.getElementsByClassName("tache");
        tab_Taches[tab_Taches.length - 1].insertAdjacentHTML("afterend", div);
    }

function init(){
	foodMenuElement = document.getElementById("food-menu");
	foodMenuElement.classList.add("hide");
	foodMenuEnabled = false;

	stockMenuElement = document.getElementById("stock-menu");
	stockMenuElement.classList.add("hide");
	stockMenuEnabled = false;

	cameraMenuElement = document.getElementById("camera-menu");
	cameraMenuElement.classList.add("hide");
	cameraMenuEnabled = false;

	checkListMenuElement = document.getElementById("checklist-menu");
	checkListMenuElement.classList.add("hide");
	checklistMenuEnabled = false;

	addListMenuElement = document.getElementById("addlist-menu");
	addListMenuElement.classList.add("hide");
	addlistMenuEnabled = false;
}

function initFirst(){
	init();

	removeClass("page1-left", "hide-page-left");
	removeClass("page1-right", "hide-page-right");
	addClass("page2-left", "hide-page-left");
	addClass("page2-right", "hide-page-right");
	currentPage = 1;
	idEnCours = 1;

}

function toggle(id){
	if(id == "food-menu"){
		toggleFoodMenu();
		return;
	}
	if(id == "stock-menu"){
		toggleStockMenu();
		return;
	}
	if(id == "camera-menu"){
		toggleCameraMenu();
		return;
	}
	if(id == "checklist-menu"){
		toggleChecklistMenu();
		return;
	}
	if(id == "addlist-menu"){
		toggleAddListMenu();
		return;
	}
}

function isSomethingOn(){
	return foodMenuEnabled  | stockMenuEnabled | cameraMenuEnabled | weatherMenuEnabled | checklistMenuEnabled | addlistMenuEnabled;
}


function toggleAddListMenu(){
	var addListMenuElement = document.getElementById("addlist-menu");
	if(addlistMenuEnabled){
		addListMenuElement.classList.add("hide");
		addlistMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		addListMenuElement.classList.remove("hide");
		addlistMenuEnabled = true;
	}
}

function toggleChecklistMenu(){
	var checkListMenuElement = document.getElementById("checklist-menu");
	if(checklistMenuEnabled){
		checkListMenuElement.classList.add("hide");
		checklistMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		checkListMenuElement.classList.remove("hide");
		checklistMenuEnabled = true;
	}
}

function toggleCameraMenu(){
	var cameraMenuElement = document.getElementById("stock-menu");
	if(cameraMenuEnabled){
		cameraMenuElement.classList.add("hide");
		cameraMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		cameraMenuElement.classList.remove("hide");
		cameraMenuEnabled = true;
	}
}

function toggleStockMenu(){
	var stockMenuElement = document.getElementById("stock-menu");
	if(stockMenuEnabled){
		stockMenuElement.classList.add("hide");
		stockMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		stockMenuElement.classList.remove("hide");
		stockMenuEnabled = true;
	}
}

function toggleFoodMenu(){
	var foodMenuElement = document.getElementById("food-menu");
	if(foodMenuEnabled){
		foodMenuElement.classList.add("hide");
		foodMenuEnabled = false;
	}else{
		if(isSomethingOn()){
			init();
		}
		foodMenuElement.classList.remove("hide");
		foodMenuEnabled = true;
	}
}


function switchPage(){
	if(currentPage == 1){
		addClass("page1-left", "hide-page-left");
		addClass("page1-right", "hide-page-right");
		removeClass("page2-left", "hide-page-left");
		removeClass("page2-right", "hide-page-right");

		currentPage = 2;
	}else{
		removeClass("page1-left", "hide-page-left");
		removeClass("page1-right", "hide-page-right");
		addClass("page2-left", "hide-page-left");
		addClass("page2-right", "hide-page-right");
		currentPage = 1;
	}
}

function addClass(paramClassPrim, paramClass){
	var classArray = document.getElementsByClassName(paramClassPrim);
	var i;
	for (i = 0; i < classArray.length; i++) {
		classArray[i].classList.add(paramClass);
	}
}

function removeClass(paramClassPrim, paramClass){
	var classArray = document.getElementsByClassName(paramClassPrim);
	var i;
	for (i = 0; i < classArray.length; i++) {
		classArray[i].classList.remove(paramClass);
	}
}
